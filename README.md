# iperf for fun :)

This repo is a document that contains some iperf commands to begin with.

## Getting Started

iperf is installable using apt on ubuntu. 

# iperf server

-s shows that the device wants to act as a server. -u shows it will be received udp packets.

```
iperf -s -u
```

# iperf client

Using the following command you can generate traffic to a previously up server. -c shows that you will act like a client. -b shows the bandwidth. -i is interval between the requests. -t is the number of the requests to be sent.

```
iperf -c <server's ip address> -b 1G -i 1 -t 1000
```
